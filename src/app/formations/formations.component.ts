import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-formations',
  templateUrl: './formations.component.html',
  styleUrls: ['./formations.component.css']
})
export class FormationsComponent implements OnInit {
    form: FormGroup;

  constructor() { }

  ngOnInit() {
    this.form = new FormGroup({
      'ref': new FormControl('', Validators.required),
      'mod': new FormControl('', Validators.required),
      'cof': new FormControl('', Validators.required),
      'nbh': new FormControl('', Validators.required),
    });
  }

  get nbh() {
    return this.form.get('nbh');
  }
  get cof() {
    return this.form.get('cof');
  }
  get ref() {
    return this.form.get('ref');
  }

  get mod() {
    return this.form.get('mod');
  }

  onSubmit() {

  }
}
