import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PageIntrouvableComponent} from './page-introuvable/page-introuvable.component';
import {AppComponent} from './app.component';


const routes: Routes = [{
  path: 'pageIntrouvable',
  component: PageIntrouvableComponent,

},
  // {
  //   path: '**',
  //   redirectTo: 'pageIntrouvable',
  // }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
