import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'examenTwin';

  affEsprit:boolean;
  affFormation:boolean;
  ngOnInit() {
    this.affEsprit=false;
    this.affFormation=false;
  }
  onClickEsprit() {
  this.affEsprit=!this.affEsprit;
    this.affFormation=!this.affEsprit;
  }
  onClickFormations() {
  this.affFormation=!this.affFormation;
    this.affEsprit=!this.affFormation;
  }


}
